import bpy,math,mathutils,random

NORMAL_TOLERANCE =         .001
DISTANCE_TOLERANCE =       .00001
FLOOR_TOLERANCE =          .01
UP =                        mathutils.Vector((0,0,1))
TARGET_COLLECTION =         'scripted'

def store_polygon( poly ):
    candidates.append( [polygon.center[0],polygon.center[1],polygon.center[2]] )

def unlink_all_collections( obj ):
    bpy.ops.object.select_all(action='DESELECT')
    obj.select_set(True)
    bpy.context.view_layer.objects.active = obj
    bpy.ops.collection.objects_remove_all()

def distp( poly, center ):
    dx = poly.center[0] - center[0]
    dy = poly.center[1] - center[1]
    return ( dx*dx+dy*dy )

candidates =                []
current_candidate =         None
max_count =                 -1
dot_threshold =             1.0 - NORMAL_TOLERANCE
min_z =                     None
curr_z =                    0

# starting by duplicate object
bpy.ops.object.duplicate_move(
    OBJECT_OT_duplicate={"linked":False, "mode":'TRANSLATION'}, 
    TRANSFORM_OT_translate={"value":(0, 0, 0)})
obj =                       bpy.context.object
mesh =                      bpy.context.object.data

# cleanup
bpy.ops.object.mode_set(mode='OBJECT')
bpy.ops.object.transform_apply(location=True, rotation=True, scale=True)
bpy.ops.object.mode_set(mode='EDIT')
bpy.ops.mesh.select_mode(use_extend=False, use_expand=False, type='VERT')
bpy.ops.mesh.select_all(action='SELECT')
bpy.ops.mesh.delete_loose()
bpy.ops.mesh.remove_doubles(threshold=DISTANCE_TOLERANCE)
bpy.ops.mesh.select_mode(use_extend=False, use_expand=False, type='FACE')
bpy.ops.mesh.select_all(action='SELECT')
bpy.ops.mesh.normals_make_consistent(inside=False)
bpy.ops.mesh.select_all(action='DESELECT')
bpy.ops.object.mode_set(mode='OBJECT')

# searching floor
for polygon in mesh.polygons:
    if min_z == None:
        min_z = polygon.center[2]
    elif min_z > polygon.center[2]:
        min_z = polygon.center[2]

# normal correction of faces above the floor
for polygon in mesh.polygons:
    pnorm = mathutils.Vector(polygon.normal)
    if abs( polygon.center[2] - min_z ) <= FLOOR_TOLERANCE:
        continue
    if pnorm.dot( UP ) <= -dot_threshold:
        polygon.select = True
        bpy.ops.object.mode_set(mode='EDIT')
        bpy.ops.mesh.flip_normals()
        bpy.ops.mesh.select_all(action='DESELECT')
        bpy.ops.object.mode_set(mode='OBJECT')

# destruction of floor and sides
for polygon in mesh.polygons:
    pnorm = mathutils.Vector(polygon.normal)
    if pnorm.dot( UP ) <= dot_threshold:
        polygon.select = True
    elif abs( polygon.center[2] - min_z ) <= FLOOR_TOLERANCE:
        polygon.select = True

bpy.ops.object.mode_set(mode='EDIT')
bpy.ops.mesh.delete(type='FACE')
bpy.ops.mesh.select_all(action='DESELECT')
bpy.ops.object.mode_set(mode='OBJECT')

# remains only top facing polygons
for polygon in mesh.polygons:
    #registration of polygon XY coordinates
    store_polygon( polygon )
    polygon.select = True

# selecting all vertices
bpy.ops.object.mode_set(mode='EDIT')
bpy.ops.mesh.select_mode(use_extend=False, use_expand=False, type='VERT')
bpy.ops.object.mode_set(mode='OBJECT')

# flattening ALL
for vertex in mesh.vertices:
    vertex.co[2] = min_z

# cleanup
bpy.ops.object.mode_set(mode='EDIT')
bpy.ops.mesh.select_mode(use_extend=False, use_expand=False, type='VERT')
bpy.ops.mesh.select_all(action='SELECT')
bpy.ops.mesh.remove_doubles(threshold=DISTANCE_TOLERANCE)
bpy.ops.mesh.select_mode(use_extend=False, use_expand=False, type='FACE')
bpy.ops.mesh.select_all(action='DESELECT')
bpy.ops.object.mode_set(mode='OBJECT')

# updating centers
valid_candidates = []
for polygon in mesh.polygons:
    z = None
    smallestd = -1
    for c in candidates:
        d = distp( polygon, c )
        if smallestd == -1 or smallestd > d:
            smallestd = d
            z = c[2]
    if z != None:
        valid_candidates.append( [polygon.center[0], polygon.center[1], z] )
candidates = valid_candidates.copy()
valid_candidates = []

# sorting the candidates based on elevation (Z)
candidates = sorted( candidates, key=lambda x: x[2] )

print( 'min: ', min_z, ', max: ', candidates[ len( candidates )-1 ][2] )
print( 'candidates: ', len( candidates ), ', polygons: ', len( mesh.polygons ) )

curr_z = min_z
total_delta = candidates[ len( candidates )-1 ][2] - min_z
total_extrusion = 0

while len(candidates) > 0:

    while len(candidates) > 0 and candidates[0][2] <= curr_z:
        candidates.pop(0)
    if len(candidates) == 0:
        break
    
    # select all faces
    ccount = 0
    for c in candidates:
        closestp = None
        smallestd = -1
        for polygon in mesh.polygons:
            if curr_z > min_z and polygon.center[2] - curr_z < -FLOOR_TOLERANCE:
                continue
            d = distp( polygon, c )
            if smallestd == -1 or smallestd > d:
                smallestd = d
                closestp = polygon
        if closestp != None:
            ccount += 1
            closestp.select = True
    
    extrusion = candidates[0][2]-curr_z
    total_extrusion += extrusion
    
    if extrusion > 0:
        bpy.ops.object.mode_set(mode='EDIT')
        bpy.ops.mesh.select_mode(use_extend=False, use_expand=False, type='FACE')
        bpy.ops.mesh.extrude_region_move(
            MESH_OT_extrude_region={
                "use_normal_flip":False, 
                "use_dissolve_ortho_edges":False, 
                "mirror":False
            },
            TRANSFORM_OT_translate={
                "value":(0,0,extrusion),
                "orient_type":'GLOBAL',
            })
        bpy.ops.mesh.select_all(action='SELECT')
        bpy.ops.mesh.normals_make_consistent(inside=False)
        bpy.ops.mesh.select_all(action='DESELECT')
        bpy.ops.object.mode_set(mode='OBJECT')
    
    curr_z = candidates[0][2]
    
    print( "{0:.3f}".format(min_z), ' > ', "{0:.3f}".format(curr_z), '(extrusion: ', "{0:.6f}".format(extrusion), ') polygons selected: ', ccount,', todo: ', len( candidates ) )
    print('polygons ', len( mesh.polygons ) )

# final cleanup
bpy.ops.object.mode_set(mode='EDIT')
bpy.ops.mesh.select_all(action='SELECT')
bpy.ops.mesh.select_mode(use_extend=False, use_expand=False, type='VERT')
bpy.ops.mesh.remove_doubles(threshold=DISTANCE_TOLERANCE)
bpy.ops.object.mode_set(mode='OBJECT')

print( 'total_delta: ', total_delta, ' <> total_extrusion: ', total_extrusion )

# and linking the copy in target collection
unlink_all_collections( obj )
bpy.data.collections[TARGET_COLLECTION].objects.link( obj )