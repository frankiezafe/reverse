tool

extends Spatial

export(String) var font_folder:String = '' setget set_font_folder
export(Vector3) var offset_mult:Vector3 = Vector3(1,0,0)
export(Vector3) var character_gap:Vector3 = Vector3(0,0,0)
export(Vector3) var white_space:Vector3 = Vector3(0,0,0)
export(Material) var material:Material = null
export(String) var text:String = '' setget set_text
export(bool) var update:bool = false setget set_update

var font:GDScript = null
var offset:Vector3 = Vector3.ZERO

func set_font_folder(s:String) -> void:
	font_folder = s
	font = load( font_folder + 'font.gd' )
	update = true

func set_text(s:String) -> void:
	text = s
	set_update( true )

func append_char( cd:Dictionary ) -> void:
	var mi:MeshInstance = MeshInstance.new()
	add_child( mi )
	mi.owner = owner
	mi.mesh = load( cd.path )
	mi.material_override = material
	mi.translation = offset
	offset += character_gap + Vector3( cd.size[0],cd.size[1],cd.size[2] ) * offset_mult

func set_update(b:bool) -> void:
	
	update = false
	
	if font == null:
		return
	while get_child_count() != 0:
		remove_child( get_child(0) )
	if text.length() == 0:
		return
	var info:Node = Node.new()
	info.set_script( font )
	add_child( info )
	info.owner = owner
	print( info.data )
	offset = Vector3.ZERO
	for i in range( 0, text.length() ):
		var c = text.substr(i,1)
		for cd in info.data.characters:
			if cd.char == c:
				append_char( cd )
				break
		if c == ' ':
			offset += white_space
