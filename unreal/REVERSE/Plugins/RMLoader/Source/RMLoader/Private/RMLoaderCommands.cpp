// Copyright Epic Games, Inc. All Rights Reserved.

#include "RMLoaderCommands.h"

#define LOCTEXT_NAMESPACE "FRMLoaderModule"

void FRMLoaderCommands::RegisterCommands()
{
	UI_COMMAND(PluginAction, "RMLoader", "Execute RMLoader action", EUserInterfaceActionType::Button, FInputGesture());
}

#undef LOCTEXT_NAMESPACE
