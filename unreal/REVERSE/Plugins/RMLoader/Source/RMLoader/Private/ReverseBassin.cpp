#include "ReverseBassin.h"

//AReverseBassin::AReverseBassin(const FRerverseMapBassin& bassin_data) {
//	
//#ifdef REVERSE_BASSIN_TICK
//	PrimaryActorTick.bCanEverTick = true;
//#endif
//
//}

// Sets default values
AReverseBassin::AReverseBassin():
	mat_top(nullptr),
	mat_side(nullptr),
	base(nullptr),
	visu01(nullptr),
	visu02(nullptr),
	visu03(nullptr)
{

#ifdef REVERSE_BASSIN_TICK
	PrimaryActorTick.bCanEverTick = true;
#endif

}

AReverseBassin::AReverseBassin(const FObjectInitializer& ObjectInitializer) :
	Super(ObjectInitializer),
	base(nullptr),
	visu01(nullptr),
	visu02(nullptr),
	visu03(nullptr)
{

	PrimaryActorTick.bCanEverTick = false;
	root = ObjectInitializer.CreateDefaultSubobject<USceneComponent>(this, TEXT("root"));
	root->SetMobility(EComponentMobility::Movable);
	SetRootComponent(root);

#ifdef REVERSE_BASSIN_TICK
	PrimaryActorTick.bCanEverTick = true;
#endif

}

// Called when the game starts or when spawned
void AReverseBassin::BeginPlay()
{
	Super::BeginPlay();

}

// Called every frame
void AReverseBassin::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

bool AReverseBassin::load_data(const FRerverseMapBassin& bassin_data) {

	display_mesh = bassin_data.display_mesh;
	display_position = RMUtils::load_vector(bassin_data.display_position);
	collision_mesh = bassin_data.collision_mesh;
	collision_position = RMUtils::load_vector(bassin_data.collision_position);

	display_name = bassin_data.display_name;
	tex_depth = bassin_data.tex_depth;
	tex_normal = bassin_data.tex_normal;

	SetActorLocation(RMUtils::load_vector(bassin_data.display_position));
	SetActorLabel("RBassin " + bassin_data.display_name);

	base = init_static_mesh(FName("base mesh"));
	visu01 = init_static_mesh(FName("visualisation 01"));
	visu02 = init_static_mesh(FName("visualisation 02"));
	visu03 = init_static_mesh(FName("visualisation 03"));

	FString dp;
	FString fp;
	FString ep;
	FPaths::Split(display_mesh, dp, fp, ep);

	FString path = FString("StaticMesh'/Game/REVERSE-map/") + fp + FString(".") + fp + FString("'");
	UStaticMesh* mesh = Cast<UStaticMesh>(StaticLoadObject(UStaticMesh::StaticClass(), nullptr, *path, nullptr, LOAD_None, nullptr));
	if (mesh) {
		base->SetStaticMesh(mesh);
	}
	else {
		UE_LOG(LogTemp, Error, TEXT("failed to load %s"), *path);
	}

	//static ConstructorHelpers::FObjectFinder<UStaticMesh> m_finder(*path);
	//if (m_finder.Succeeded()) {
	//	base->SetStaticMesh(m_finder.Object);
	//} else {
	//	UE_LOG(LogTemp, Error, TEXT("failed to load %s"), *display_mesh);
	//}

	return true;

}

UStaticMeshComponent* AReverseBassin::init_static_mesh(const FName& name) {

	UStaticMeshComponent*  scm = NewObject<UStaticMeshComponent>(this, name);
	scm->SetupAttachment(GetRootComponent());
	scm->CreationMethod = EComponentCreationMethod::Native;
	scm->RegisterComponent();
	return scm;

}

void AReverseBassin::set_materials(UMaterialInterface* top, UMaterialInterface* side) {
	
	if (mat_top == top && mat_side == side) {
		return;
	}

	if (top == nullptr) {
		mat_top = nullptr;
	}
	else {
		//mat_top = UMaterialInstanceDynamic::Create(top, this);
		mat_top = top;
	}

	if (side == nullptr) {
		mat_side = nullptr;
	}
	else {
		//mat_side = UMaterialInstanceDynamic::Create(side, this);
		mat_side = side;
	}

	if (base) {
		UStaticMesh* base_mesh = base->GetStaticMesh();
		if (base_mesh) {
			base_mesh->SetMaterial(0, mat_side);
			base_mesh->SetMaterial(1, mat_top);
		}
	}

}